from django.contrib.auth.models import User
from django.db import models


class Register(models.Model):
    record_date = models.DateField(auto_now_add=True)
    modify_date = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)

    class Meta:
        abstract = True


class MyModel(Register):
    name = models.CharField(max_length=255)
    lastname = models.CharField(max_length=255)
    dni = models.IntegerField()
    address = models.CharField(max_length=100)
    phone = models.IntegerField()
    sex = models.CharField(max_length=15)
    birthday = models.DateField()

    class Meta:
        abstract = True


class Patient(MyModel):
    #record_patient = models.ForeignKey(User, related_name='record_patient', on_delete=models.DO_NOTHING)
    #modify_patient = models.ForeignKey(User, related_name='modify_patient', on_delete=models.DO_NOTHING)

    def __str__(self):
        return f'{self.name} {self.lastname}'


class Medical(MyModel):
    #record_medical = models.ForeignKey(User, related_name='record_medical', on_delete=models.DO_NOTHING)
    #modify_medical = models.ForeignKey(User, related_name='modify_medical', on_delete=models.DO_NOTHING)
    email = models.EmailField()
    num_college = models.IntegerField()

    def __str__(self):
        return f'{self.name} {self.lastname}'


class Specialty(Register):
    #record_specialty = models.ForeignKey(User, related_name='record_specialty', on_delete=models.DO_NOTHING)
    #modify_specialty = models.ForeignKey(User, related_name='modify_specialty', on_delete=models.DO_NOTHING)
    name = models.CharField(max_length=100)
    description = models.TextField()

    def __str__(self):
        return self.name


class Appointment(Register):
    medical = models.ForeignKey(Medical, on_delete=models.CASCADE)
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    attention_date = models.DateField()
    attention_start = models.CharField(max_length=5)
    attention_end = models.CharField(max_length=5)
    state = models.CharField(max_length=25)
    observations = models.TextField()
    #record_appointment = models.ForeignKey(User, related_name='record_appointment', on_delete=models.DO_NOTHING)
    #modify_appointment = models.ForeignKey(User, related_name='modify_appointment', on_delete=models.DO_NOTHING)

    def __str__(self):
        return f'{self.patient.name} {self.patient.lastname}--{self.attention_start} a {self.attention_end}'


class Schedule(Register):
    medical = models.ForeignKey(Medical, on_delete=models.CASCADE)
    attention_date = models.DateField()
    attention_start = models.CharField(max_length=5)
    attention_end = models.CharField(max_length=5)
    #record_schedule = models.ForeignKey(User, related_name='record_schedule', on_delete=models.DO_NOTHING)
    #modify_schedule = models.ForeignKey(User, related_name='modify_schedule', on_delete=models.DO_NOTHING)

    def __str__(self):
        return f'{self.medical.name} {self.medical.lastname}--{self.attention_start} a {self.attention_end}'


class MedicalSpecialty(Register):
    medical = models.ForeignKey(Medical, on_delete=models.CASCADE)
    specialty = models.ForeignKey(Specialty, on_delete=models.CASCADE)
    #record_medical_specialty = models.ForeignKey(User, related_name='record_medical_specialty',on_delete=models.DO_NOTHING)
    #modify_medical_specialty = models.ForeignKey(User, related_name='modify_medical_specialty', on_delete=models.DO_NOTHING)

    def __str__(self):
        return f'{self.specialty.name} --{self.medical.name} {self.medical.lastname}--'
