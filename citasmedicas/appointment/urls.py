from django.urls import path
from rest_framework import routers
from appointment.views import *
router = routers.DefaultRouter()
router.register('specialty', SpecialtyViewSet)
router.register('medical', MedicalViewSet)
router.register('patient', PatientViewSet)
router.register('schedule', ScheduleViewSet)
router.register('appointment', AppointmentViewSet)
router.register('medicalspecialty', MedicalSpecialtyViewSet)

urlpatterns = [
    path('specialty/<int:pk>', specialty_detail, name='specialty_detail'),
    path('medical/<int:pk>', medical_detail, name='medical_detail'),
    path('patient/<int:pk>', patient_detail, name='patient_detail'),
    path('schedule/<int:pk>', schedule_detail, name='schedule_detail'),
    path('appointment/<int:pk>', appointment_detail, name='appointment_detail'),
    path('medicalspecialty/<int:pk>', specialty_detail, name='medicalspecialty_detail'),
]
