from django.contrib import admin
from appointment.models import *


@admin.register(Medical)
class MedicalAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'lastname']


@admin.register(Patient)
class PatientAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'lastname']


@admin.register(Specialty)
class SpecialtyAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'description', 'record_date', 'modify_date', 'active']


@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    list_display = ['id', 'medical', 'patient', 'record_date', 'modify_date', 'active']


@admin.register(Schedule)
class ScheduleAdmin(admin.ModelAdmin):
    list_display = ['id', 'medical', 'attention_start', 'attention_end', 'record_date', 'modify_date', 'active']


@admin.register(MedicalSpecialty)
class MedicalSpecialtyAdmin(admin.ModelAdmin):
    list_display = ['id', 'medical', 'specialty', 'record_date', 'modify_date', 'active']
