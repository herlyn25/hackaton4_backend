from rest_framework import serializers
from appointment.models import *

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username']

class SpecialtySerializer(serializers.ModelSerializer):
    class Meta:
        model = Specialty
        fields = ['id', 'name', 'description', 'record_date', 'modify_date', 'active']

    def to_representation(self, instance):
        self.fields['record_specialty'] = UserSerializer(read_only=True)
        self.fields['modify_specialty'] = UserSerializer(read_only=True)
        return super().to_representation(instance)


class MedicalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Medical
        fields = ['id', 'name', 'lastname', 'dni', 'address', 'phone', 'sex', 'birthday', 'email',
                  'num_college', 'record_date', 'modify_date', 'active']

    def to_representation(self, instance):
        #self.fields['record_medical'] = UserSerializer(read_only=True)
        #self.fields['modify_medical'] = UserSerializer(read_only=True)
        return super().to_representation(instance)

class PatientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Patient
        fields = ['id', 'name', 'lastname', 'dni', 'address', 'phone', 'sex', 'birthday',
                  'record_date', 'modify_date', 'active']

    def to_representation(self, instance):
        #self.fields['record_patient'] = UserSerializer(read_only=True)
        #self.fields['modify_patient'] = UserSerializer(read_only=True)
        return super().to_representation(instance)

class AppointmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Appointment
        fields = ['id', 'medical', 'patient', 'attention_date', 'attention_start', 'attention_end',
                  'state', 'observations', 'record_date','modify_date', 'active']

    def to_representation(self, instance):
        self.fields['medical'] = MedicalSerializer(read_only=True)
        self.fields['patient'] = PatientSerializer(read_only=True)
        #self.fields['record_appointment'] = UserSerializer(read_only=True)
        #self.fields['modify_appointment'] = UserSerializer(read_only=True)
        return super().to_representation(instance)


class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = ['id', 'medical', 'attention_date', 'attention_start', 'attention_end',
                  'record_date', 'modify_date', 'active']

    def to_representation(self, instance):
        self.fields['medical'] = MedicalSerializer(read_only=True)
        #self.fields['modify_schedule'] = UserSerializer(read_only=True)
        #self.fields['record_schedule'] = UserSerializer(read_only=True)
        return super().to_representation(instance)

class MedicalSpecialtySerializer(serializers.ModelSerializer):
    class Meta:
        model = MedicalSpecialty
        fields = ['id', 'medical', 'specialty', 'record_date', 'modify_date','active']

    def to_representation(self, instance):
        self.fields['medical'] = MedicalSerializer(read_only=True)
        self.fields['specialty'] = SpecialtySerializer(read_only=True)
        #self.fields['record_medical_specialty'] = UserSerializer(read_only=True)
        #self.fields['modify_medical_specialty'] = UserSerializer(read_only=True)
        return super().to_representation(instance)
